# GitLab CI for iOS

This is a sample project created to explore and understand how GitLab's CI and 
the GitLab Runner work.

Visit the [_Builds_](https://gitlab.com/AngeloStavrow/gitlab-ci-for-ios-projets/builds) 
page to see the results of my experiments with the [`.gitlab-ci.yml` file](https://gitlab.com/AngeloStavrow/gitlab-ci-for-ios-projects/blob/master/.gitlab-ci.yml).